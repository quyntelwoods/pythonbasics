Omada Foititwn
===============
Π09049 Kalogeropoulos Giannis
Π09134 Tzanetos Nikolaos
Π09016 Aspiotis Gewrgios-Marios


Παρακαλώ ανατρέξατε στο αρχείο .docx στο mail που αποστείλαμε (μέσα στο pythonbasics folder)
 για βασική περιγραφή της εργασίας και στη συνέχεια στις αντίστοιχες κλάσεις για στοχευμένο σχολιασμό.

Ομάδα Φοιτητών
===============
Π09049 Καλογερόπουλος Ιωάννης
Π09134 Τζανέτος Νικόλαος
Π09016 Ασπιώτης Γεώργιος-Μάριος

Desc.
===============
This web application acts as an information system for a cellphone carrier.

It allows administrators for the system, sellers of the company and clients to
login and perform different operations/queries to the system and the db depending
on their role.

Administrators can add/remove sellers and clients to/from the system and
create new telephony programs.

Sellers can create new clients, print bills of clients and change the telephony
programs of clients depending on the exising programs.

Clients can see their account info, examine their call history and execute
payments regarding their account.  
