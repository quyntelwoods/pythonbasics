from sqlalchemy import create_engine, select,update
from sqlalchemy.orm import sessionmaker
from mainpackage.dao import User, Telephony_Program, Bill
from passlib.handlers.sha2_crypt import sha256_crypt
from mainpackage.UserClasses.Client import Client

class Seller(object):
    "Subclass Seller of Users"
    def __init__(self, _session):
        self.session = _session

    def ClientCreationFromSeller(self, username_client, password_client, email_client, firstname_client, lastname_client, afm_client):
        try:
            #gnwrizoume oti to role_id = 1 gia pelates
            self.session.add(User(username = username_client, password = str(sha256_crypt.encrypt(password_client, rounds = 20000, salt_size=16)),
                                        email = email_client, firstname = firstname_client,
                                        lastname = lastname_client, afm = afm_client, role_id = 3, flag = True))
            self.session.commit()
        except Exception as e:
            raise

    def printBill(self, client_id_arg):
        "Client getAccountInfo2 callback since the two methods are essentially the same"
        c = Client(_session = self.session)
        info = c.getAccountInfo2(client_id_arg)
        return info


    def changeClientProgram(self, teleph_number_arg, program_id_arg):
        try:
            q = self.session.query(Bill).filter(Bill.teleph_number == teleph_number_arg)
            for entry in q:
                entry.program_id = program_id_arg
            self.session.commit()
        except Exception as e:
            raise
