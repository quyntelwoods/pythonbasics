from sqlalchemy import create_engine, select
from sqlalchemy.orm import sessionmaker
from mainpackage.UserClasses.AdminClass import Admin
from mainpackage.UserClasses.Client import Client
from mainpackage.UserClasses.Seller import Seller
from mainpackage.dao import User, Bill, Telephony_Program, Call
from passlib.handlers.sha2_crypt import sha256_crypt
from mainpackage.Helpers import Helper
import unittest2 as unittest
from sqlalchemy import text
from datetime import datetime

class TestQueries(unittest.TestCase):
    '''
    we add the rows that we want to test in the setUp portion of the test
    and delete them in the tearDown
    '''
    def setUp(self):
        engine = create_engine("postgresql://kiniti_user:14mTh3L4w!@83.212.99.115/kiniti_tilefwnia", echo=True)
        self.Session = sessionmaker(bind=engine)
        self.session = self.Session()

    def tearDown(self):
        self.Session.close_all()


    def dont_test_Client_getAccountInfo_part1(self):
        client_id = 5
        stmt1 = text (  """
                        select bill.teleph_number, users.firstname, users.lastname, telephony_program.charge_mob, telephony_program.charge_home, telephony_program.charge_standard, telephony_program.program_desc
                        from users
                        inner join bill
                        on users.user_id = bill.user_id
                        inner join telephony_program
                        on bill.program_id = telephony_program.program_id
                        where users.user_id =:id;
                        """)

        stmt1 = stmt1.columns(Bill.teleph_number, User.firstname, User.lastname,
                            Telephony_Program.charge_mob, Telephony_Program.charge_home,
                            Telephony_Program.charge_standard, Telephony_Program.program_desc)

        result_info = self.session.query(Bill.teleph_number, User.firstname, User.lastname,
                            Telephony_Program.charge_mob, Telephony_Program.charge_home,
                            Telephony_Program.charge_standard,
                            Telephony_Program.program_desc).from_statement(stmt1).params(id=client_id).all()
        print result_info
        print (result_info[0].charge_mob, result_info[0].charge_home, result_info[0].charge_standard)

    def dont_test_Client_getAccountInfo_part2(self):
        client_id = 5
        stmt2 = text(
            """
            select call.call_dur, call.called_number, call.call_date
            from users inner join bill
            on users.user_id = bill.user_id
            inner join call
            on call.bill_id = bill.bill_id
            where users.user_id =:id and extract (month from call.call_date) =:month;
            """
        )

        stmt2 = stmt2.columns(Call.call_dur, Call.called_number, Call.call_date)

        #pairnoume ton logariasmo tou proigoumenou mina mono
        result_calls = self.session.query(Call.call_dur, Call.called_number,
                                    Call.call_date).from_statement(stmt2).params(id = client_id, month = datetime.now().month - 1).all()

        print result_calls

    def test_Client_getAccountInfo2(self):
        c = Client(self.session)
        res = c.getAccountInfo2(5)
        print res
        print res[1]

    def dont_test_changes_on_add_methods_and_classes(self):
        '''testing the query methods in the database by querying the database but not deleting'''
        #arrange
        # a = Admin(self.session)
        # a.createClient('user1', 'pass1', 'email1@mail.com', 'name1', 'lastname1', '12121212')
        # h = Helper()
        # h.createBill(self.session, '6993090901', 2, 30)
        # h.createBill(self.session, '0909019991', 1, 30)
        #act
        self.session.query(Bill).filter(Bill.user_id == 30).delete()
        self.session.query(User).filter(User.user_id == 30, User.role_id == 3).delete()
        self.session.commit()
        bill_results = self.session.query(Bill).filter(Bill.user_id == 30).all()
        user_results = self.session.query(User).filter(User.user_id == 30, User.role_id == 3).all()
        #assert
        self.assertEqual(user_results[0].username, 'user1')
        for user in user_results:
            print (user.user_id, user.username, user.password, user.email,
                   user.firstname, user.lastname, user.afm)

        for bill in bill_results:
            print (bill.teleph_number, bill.program_id, bill.user_id)
        # self.assertEqual(bill_results[1], '0909019991')
        self.assertEqual(bill_results[0].teleph_number, '6993090901')
        print bill_results
        print bill_results[0].teleph_number

    def dont_test_Helper_getLastBillId(self):
        h = Helper()
        bId_received = h.getLastBillId(_session=self.session, user_id_arg=9)
        print (bId_received)
        self.assertEqual(bId_received, 27)

    def dont_test_Client_payment(self):
        c = Client(_session = self.session)
        a = c.payment(7, 2)
        #overall_cost_list = self.session.query(Bill.bill_overall_due, Bill.bill_id).filter(Bill.bill_id == bill_id).order_by(Bill.bill_id.desc()).first()
        #overall_cost = overall_cost_list[0]
        #self.assertEqual(get_overall_due, 1)
        print a

    def dont_test_Seller_changeClientProgram(self):
        s = Seller(_session = self.session)
        s.changeClientProgram("6947673888", 2)

    def dont_test_Admin_removeClient(self):
        a = Admin(_session = self.session)
        a.removeClient(12)
        print "client removed"

    def dont_test_Admin_removeSeller(self):
        a = Admin(_session = self.session)
        a.removeSeller(14)
        print "client removed"

    def test_create_admin(self):
        self.session.add(User(username = 'death', password = str(sha256_crypt.encrypt('star', rounds = 20000, salt_size=16)),
                                      email = 'palpy@council.gxy', firstname = 'sheev',
                                      lastname = 'palpatine', afm = '99999999', role_id = 1, flag = True))
        self.session.commit()

suite = unittest.TestLoader().loadTestsFromTestCase(TestQueries)
unittest.TextTestRunner(verbosity=2).run(suite)



# session.query(Bill).filter(Bill.user_id ==user_id).delete()
# session.query(User).filter(User.user_id == user_id, User.role_id = 3).delete()
# session.commit()
