#!/usr/bin/env python
#tha xrisimopoihsoume flask kai postgres gia tin ergasia
#min xehasoume na kleisoume ta sessions

from flask import Flask, render_template, flash, request, url_for, redirect, session
from passlib.hash import sha256_crypt
from wtforms import Form, BooleanField, TextField, PasswordField, validators
import gc

app = Flask(__name__)

@app.route("/")
def index():
    return "<h1>Index Page</h1>"

@app.route("/admin")
def administer():

    return "<h2>Administrators page</h1>"

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)



if __name__ == "__main__":
    app.run(threaded = True, debug = True)
