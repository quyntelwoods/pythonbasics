#!/usr/bin/env python
#tha xrisimopoihsoume flask kai postgres gia tin ergasia
#min xehasoume na kleisoume ta sessions

from flask import Flask, render_template, request


app = Flask(__name__)

@app.route("/")
def index():
    return "<h1>Index Page</h1>"

@app.route("/admin")
def administer():

    return "<h2>Administrators page</h1>"

@app.route("/login", methods = ['GET', 'POST'])
def login():
    if request.method == 'POST':
        #log user in
        pass
    else:
        #show login form
        pass
    return render_template("login.html")


if __name__ == "__main__":
    app.run(threaded = True, debug = True)
