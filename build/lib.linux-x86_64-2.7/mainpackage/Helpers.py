import re
from mainpackage.dao import Bill, Call
from datetime import datetime, timedelta

class Helper(object):
    "Class which contains helper methods"
    # def __init__(self, _session):
    #     self.session = _session

    def calculateOverallAccountCost(self, calls, mobile_cost, home_cost, standard_cost):
        """
        Gets a list of calls and the charge per call type and calculates overall cost,
        by discerning whether the number is that of a mobile or a home number.
        """
        #User.user_id, Bill.bill_id, Call.call_dur, Call.called_number ,Call.call_date

        actual_calls = []
        for entry in calls:
            actual_calls.append(str(entry[3]))

        calls_durations = []
        for entry in calls:
            # entry[2] represents the duration of the call.
            # it is a timedelta object, so we are using the timedelta.total_seconds()
            # method to get a float number that represents the duration of the
            # call in seconds
            a = entry[2].total_seconds()
            calls_durations.append(a)

        overall_cost = float(standard_cost)
        i = 0

        for call in actual_calls:
            # pairnoume ton arithmo apo to array pou ms dinetai gt h client stelnei array
            # kai an o arithmos einai stathero exoume pol/smo duration me home_cost
            if (re.match('^2', call[i])):
                overall_cost = float(overall_cost) + (calls_durations[i]* float(home_cost))
            else:
                overall_cost = float(overall_cost) + (calls_durations[i]* float(mobile_cost))
            i = i + 1

        return overall_cost

    #needs updating as new columns where added
    def createBill(self, _session, teleph_number_arg, program_id_arg, user_id_arg):
        '''
        Method that adds bills to the corresponding database table.
        Didn't have where else to put it so we put it HERE (1-0)
        '''
        try:
            session = _session
            session.add(Bill(teleph_number = teleph_number_arg, program_id = program_id_arg,
                             user_id = user_id_arg))
            session.commit()
        except Exception as e:
            raise


    #get last bill_id based on user_id
    def getLastBillId(self, _session, user_id_arg):
        """
        Gets the last bill based on user_id.
        """
        try:
            session = _session
            # queries return lists for some reason. I just want the result (which will
            # always be one since I am using first())
            bill_id_list = session.query(Bill.bill_id).filter(Bill.user_id == user_id_arg).order_by(Bill.bill_id.desc()).first()
            bill_id = bill_id_list[0]
        except Exception as e:
            raise

        return bill_id
