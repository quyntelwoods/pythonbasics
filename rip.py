#!/usr/bin/env python
#tha xrisimopoihsoume flask kai postgres gia tin ergasia
#min xehasoume na kleisoume ta sessions

# passing arguments to action methods: http://stackoverflow.com/questions/26954122/python-flask-passing-arguments-into-redirecturl-for

import sigleton
from flask import Flask, request, session, g, redirect, url_for, abort, \
render_template, flash
from passlib.hash import sha256_crypt
from wtforms import Form, BooleanField, TextField, PasswordField, validators
from mainpackage.dao import User, Role
from passlib.handlers.sha2_crypt import sha256_crypt
from mainpackage.UserClasses.AdminClass import Admin
from mainpackage.UserClasses.Client import Client
from mainpackage.UserClasses.Seller import Seller


app = Flask(__name__)
#pairnoume to session tis vasis
s = sigleton.Sigleton()
parseval = s.databaseConnection()

@app.route("/testing")
def testing ():
    return render_template('testing.html', error=error)

@app.route('/client', methods = ['GET', 'POST'])
@app.route("/client/<user_id_arg>", methods = ['GET', 'POST'])
def client(user_id_arg = None):
    error = None
    c = Client(parseval)
    if request.method == 'POST':
        #checkaroume an exei upovlithei i swsti forma me to button name
        if 'sai' in request.form:
            #pairnoume to list twn pliroforiwn tou client...
            uid = int(user_id_arg)
            c_info_list = c.getAccountInfo2(uid)
            return render_template('client.html',error=error, client_info = c_info_list)
        elif 'sch' in request.form and request.form['bill_id'] != None and request.form['bill_id'] != "":
            bill_id = request.form['bill_id']
            date = ""
            teleph_number = ""
            if request.form['date'] != None and request.form != "":
                date = request.form['date']
            if request.form['called_number'] != None and request.form != "":
                teleph_number = request.form['called_number']
            c_call_history_list = c.showCallHistory(bill_id_filter = bill_id, date_filter = date, called_number_filter = teleph_number)
            return render_template('client.html',error=error, call_history = c_call_history_list)
        # h plirwmi ekteleitai sto column bill_overall_due tou pinaka bill kai logw twn dedomenwn pou exoun mpei xeirokinita
        # ston pinaka den exei panta sxesi me to getAccountInfo2
        elif 'ep' in request.form and request.form['user_id'] != None and request.form['user_id'] != "" and\
        request.form['pay_amt'] != None and request.form != "":
            user_id = str(request.form['user_id'])
            pay_amt = int(request.form['pay_amt'])
            c.payment(user_id_arg2=user_id, payment_amt=pay_amt)
            flash("Payment has been EXECUTED!!!")
            return render_template('client.html',error=error)
        else:
            error = 'Ta dedomena pou eisagate eixan sfalma'
            return render_template('client.html',error=error)
    else:
        return render_template('client.html',error=error)

@app.route("/seller",methods=['GET', 'POST'])
def seller():
    error=None
    s = Seller(_session = parseval)
    if request.method == 'POST':
        if 'cc' in request.form and request.form['username'] != None and request.form['username'] != "" and\
        request.form['password'] != None and request.form['password'] != "" and request.form['email'] != None and request.form['email'] != "" and\
        request.form['firstname'] != None and request.form['firstname'] != "" and request.form['lastname'] and request.form['lastname'] and\
        request.form['afm'] != None and request.form['afm'] != "":
            username = str(request.form['username'])
            password = str(request.form['password'])
            email = str(request.form['email'])
            firstname = str(request.form['firstname'])
            lastname = str(request.form['lastname'])
            afm = str(request.form['afm'])
            s.ClientCreationFromSeller(username_client = username, password_client = password, email_client = email, firstname_client = firstname, lastname_client = lastname, afm_client = afm)
            flash('The client has been admitted to the database!')
            return render_template('seller.html',error=error)
        elif 'pb' in request.form and request.form['user_id'] != None and request.form['user_id'] != "":
            user_id = str(request.form['user_id'])
            b_info_list = s.printBill(user_id)
            return render_template('seller.html',error=error, bill_info = b_info_list)
        elif 'chp' in request.form and request.form['teleph_number'] != None and request.form['teleph_number'] != "" and\
        request.form['program_id'] != None and request.form['program_id'] != "":
            teleph_number = str(request.form['teleph_number'])
            program_id = str(request.form['program_id'])
            s.changeClientProgram(teleph_number_arg=teleph_number, program_id_arg=program_id)
            flash('The program has been changed!')
            return render_template('seller.html', error=error)
        else:
            error ='The data you have entered are missing or wrong!'
    else:
        return render_template('seller.html',error=error)

@app.route("/admin",methods=['GET', 'POST'])
def administer():
    error=None
    a = Admin(_session = parseval)
    if request.method == 'POST':
        if 'cc' in request.form and request.form['username'] != None and request.form['username'] != "" and\
        request.form['password'] != None and request.form['password'] != "" and request.form['email'] != None and request.form['email'] != "" and\
        request.form['firstname'] != None and request.form['firstname'] != "" and request.form['lastname'] and request.form['lastname'] and\
        request.form['afm'] != None and request.form['afm'] != "":
            username = str(request.form['username'])
            password = str(request.form['password'])
            email = str(request.form['email'])
            firstname = str(request.form['firstname'])
            lastname = str(request.form['lastname'])
            afm = str(request.form['afm'])
            a.createClient(username_client = username, password_client = password, email_client = email, firstname_client = firstname, lastname_client = lastname, afm_client = afm)
            flash('The client has been admitted to the database!')
            return render_template('admin.html',error=error)
        elif 'cs' in request.form and request.form['username'] != None and request.form['username'] != "" and\
        request.form['password'] != None and request.form['password'] != "" and request.form['email'] != None and request.form['email'] != "" and\
        request.form['firstname'] != None and request.form['firstname'] != "" and request.form['lastname'] and request.form['lastname'] and\
        request.form['afm'] != None and request.form['afm'] != "":
            username = str(request.form['username'])
            password = str(request.form['password'])
            email = str(request.form['email'])
            firstname = str(request.form['firstname'])
            lastname = str(request.form['lastname'])
            afm = str(request.form['afm'])
            a.createSeller(username_seller = username, password_seller = password, email_seller = email, firstname_seller = firstname, lastname_seller = lastname, afm_seller = afm)
            flash('The seller has been admitted to the database!')
            return render_template('admin.html',error=error)
        elif 'ctp' in request.form and request.form['program_desc'] != None and request.form['program_desc'] != "" and\
        request.form['mob_charge'] != None and request.form['mob_charge'] != "" and request.form['home_charge'] != None and request.form['home_charge'] != "" and\
        request.form['stan_charge'] != None and request.form['stan_charge'] != "":
            program_desc = str(request.form['program_desc'])
            mob_charge = float(request.form['mob_charge'])
            home_charge = float(request.form['home_charge'])
            stan_charge = float(request.form['stan_charge'])
            a.createTelephonyProgram(program_description=program_desc, charge_mobile_phones=mob_charge, charge_home_phones=home_charge, fixed_mobile=stan_charge)
            flash('The program has been admitted to the database!')
            return render_template('admin.html',error=error)
        elif 'dc' in request.form and request.form['client_id'] != None and request.form['client_id'] != "":
            client_id = str(request.form['client_id'])
            a.removeClient(user_id_arg=client_id)
            flash('The client has beed disabled!')
            return render_template('admin.html',error=error)
        elif 'ds' in request.form and request.form['seller_id'] != None and request.form['seller_id'] != "":
            seller_id = str(request.form['seller_id'])
            a.removeSeller(user_id_arg=seller_id)
            flash('The seller has beed disabled!')
            return render_template('admin.html',error=error)
        else:
            error ='The data you have entered are missing or wrong!'
    else:
        return render_template('admin.html',error=error)

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        a = getuser(parseval,request.form['username'])
        if request.form['username'] == a.username and sha256_crypt.verify(request.form['password'], a.password) and a.role_id==1 and a.flag == True:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('administer'))

        elif request.form['username'] == a.username and sha256_crypt.verify(request.form['password'], a.password) and a.role_id==2 and a.flag == True:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('seller'))

        elif request.form['username'] == a.username and sha256_crypt.verify(request.form['password'], a.password) and a.role_id==3 and a.flag == True:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('client'))
        else:
            error='Invalid username or password'
    return render_template('login.html', error=error)

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('login'))

#methodos redirect
def getuser(dbsession,parameter1):
   return    dbsession.query(User).filter(User.username == parameter1).first()

# ??? http://stackoverflow.com/questions/26080872/secret-key-not-set-in-flask-session
if __name__ == "__main__":
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'


    app.debug = True
    app.run(threaded = True, debug = True)

# methodos pou pairnei to user_id apo to url
# @app.route('/client', methods = ['GET', 'POST'])
# @app.route("/client/<user_id_arg>", methods = ['GET', 'POST'])
# def client(user_id_arg = None):
#     error = None
#     c = Client(parseval)
#     g = c
#     if user_id_arg != None:
#         pass
#         #flask.g = user_id_arg
#     if request.method == 'POST':
#         #checkaroume an exei upovlithei i swsti forma me to button name
#         if 'sai' in request.form and user_id_arg != None:
#             client_info = ['a', 'a', 'a', 'a', 'a']
#             holder_user_id = user_id_arg
#             #pairnoume to list twn pliroforiwn tou client...
#
#             c_info_list = c.getAccountInfo2(user_id_arg)
#             return redirect(url_for('client', user_id_arg = holder_user_id))
#             return render_template('client.html',error=error, holder_user_id = user_id_arg, client_info = c_info_list)
#         else:
#             return redirect(url_for('login'))
#     return render_template('client.html',error=error, holder_user_id = user_id_arg, client_info = ['a', 'a', 'a', 'a', 'a'] )


# @app.route('/client', methods = ['GET', 'POST'])
# def client():
#     error = None
#     c = Client(parseval)
#     if request.method == 'POST':
#         #checkaroume an exei upovlithei i swsti forma me to button name
#         if 'sai' in request.form and request.form['user_id'] != None and request.form['user_id'] != "":
#             #pairnoume to list twn pliroforiwn tou client...
#             uid = int(request.form['user_id'])
#             c_info_list = c.getAccountInfo2(uid)
#             return render_template('client.html',error=error, client_info = c_info_list)
#         elif 'sch' in request.form and request.form['bill_id'] != None and request.form['bill_id'] != "":
#             bill_id = request.form['bill_id']
#             date = ""
#             teleph_number = ""
#             if request.form['date'] != None and request.form != "":
#                 date = request.form['date']
#             if request.form['called_number'] != None and request.form != "":
#                 teleph_number = request.form['called_number']
#             c_call_history_list = c.showCallHistory(bill_id_filter = bill_id, date_filter = date, called_number_filter = teleph_number)
#             return render_template('client.html',error=error, call_history = c_call_history_list)
#         elif 'ep' in request.form and request.form['user_id']:
#             pass
#         else:
#             error = 'Ta dedomena pou eisagate eixan sfalma'
#             return render_template('client.html',error=error)
#     else:
#         return render_template('client.html',error=error)
